Git Auto Commit
============

This thing will auto commit everything in your working directory to whatever git repo you transplant it to. Read the warnings if you get any and follow those directions.

### Version
0.0.1


### Tech

Git Auto Commit uses a number of open source projects to work properly:

* [node.js] - evented I/O for the backend
* [Gulp] - the streaming build system


### Installation
Clone the repo to the root of your working directory:
```
git clone git@bitbucket.org:alvinycheung/git-auto-commit.git
```

Install Dependencies:
```
brew install node;
npm i;
```

Run with this command:

```
gulp watch;
```