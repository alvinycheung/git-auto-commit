var gulp = require('gulp');
var watch = require('gulp-watch');
var shell = require('gulp-shell');

gulp.task('commit', [], shell.task([
  'git add .',
  'git status | grep : | sed -n \'1!p\'> ./tmp/commit.txt',
  'git commit -F ./tmp/commit.txt',
  'git push'
]));

gulp.task('watch', function () {
  gulp.watch(['*', '!tmp/', '!tmp/*'], ['commit']);
});